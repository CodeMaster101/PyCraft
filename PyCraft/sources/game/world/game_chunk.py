"""
Cette fonction contient le chunk, une classe permettant de lier un array
(issu d'un fichier) à des chunkbatchs du côté rendering
Ceci permet donc effectivement d'afficher le terrain généré à l'écran
"""
import game.settings as settings
import game.rendering.chunk_batch as chunk_batch

import random


class Chunk:
    def __init__(self, position, block_array):
        """
        La classe chunk permet de manipuler des blocs aisément en manipulant des tronçons (chunk en anglais)
        :param position: la position du chunk en coordonnées chunks
        :param block_array: un np.array contenant les blocs (x,y,z,id) 
        """
        self.position = position
        self.blocks = set()
        for block in block_array:  # Ce gueux d'Arthur a de vilains duplicats
            self.blocks.add(tuple(block))
        # les indices dans le chunkbatch de chacun des blocs (chunk_batch_local_id, cube_in_batch_id)
        # avec le chunk_batch_local_id partant de 0 à n pour le nième chunkbatch crée
        self.cube_indices = []
        self.cubes_dynamic = []  # Les blocs correctement affichables, et différents de l'air
        # Le set des cubes_dynamic, utilisé pour effectuer "in", qui est plus rapide avec des ensembles
        self.cubes_dynamic_set = set()
        self.chunkbatches = []  # Les différents chunkbatches du chunk
        self.renderer = None
        self.total_space = 0  # L'espace total disponible, en blocs
        self.indices = set()  # Les indices des chunkbatchs dans leur scène

    def load_to_screen(self, renderer):
        """
        Met les cubes dans un chunkbatch
        :param renderer: le renderer dans lequel mettre les blocs
        :return self.index: l'indice du chunkbatch
        """
        self.renderer = renderer
        # On crée le chunkbatch
        self.chunkbatches.append(chunk_batch.ChunkBatch(
            renderer.ctx, renderer.shader_manager["cube"], self.position, len(self.blocks)+settings.CHUNK_BLOCK_MARGIN))
        batch = []  # Le batch que nous mettrons dans le chunkbatch
        for cube in self.blocks:
            if cube[3] in settings.id_vers_tex:  # Le bloc a un id non corrompu
                self.cubes_dynamic.append(cube)
                self.cubes_dynamic_set.add(cube)
                batch.append((int(cube[0]), int(cube[1]), int(
                    cube[2]), settings.id_vers_tex[cube[3]]))
            elif cube[3] == 0:  # C'est de l'ait
                continue
            else:
                self.cubes_dynamic.append(cube)
                self.cubes_dynamic_set.add(cube)
                batch.append((int(cube[0]), int(cube[1]), int(
                    cube[2]), random.randint(0, 7)))

        # On récupère les indices des cubes
        self.cube_indices = [(0, i)
                             for i in self.chunkbatches[-1].push_cubes(batch)]
        # On récupère l'espace total disponible
        self.total_space = len(self.blocks)+settings.CHUNK_BLOCK_MARGIN

        self.indices.add(renderer.scene.add_chunk_batch(
            self.chunkbatches[-1]))  # On ajoute le chunkbatch

    def add_block(self, block):
        """
        Ajoute un bloc au chunkbatch
        :param block: le tuple (relx, rely, z, type)
        """

        # Il nous reste de l'espace, le -8 est dû à un bug assez étrange (voir la doc)
        if len(self.cube_indices) < self.total_space-8:
            print("RIEMANN", len(self.cube_indices), self.total_space)
            if block[3] in settings.id_vers_tex:  # Le bloc a un id existant
                index = self.chunkbatches[-1].add_cube(int(block[0]), int(
                    block[1]), int(block[2]), settings.id_vers_tex[block[3]])
            else:
                index = self.chunkbatches[-1].add_cube(int(block[0]), int(
                    block[1]), int(block[2]), random.randint(0, 7))
            # On ajoute le bloc au chunkbatch
            self.cube_indices.append((len(self.chunkbatches)-1, index))
            self.cubes_dynamic.append(block)
            self.cubes_dynamic_set.add(block)
        else:  # On crée un nouveau chunkbatch
            self.chunkbatches.append(chunk_batch.ChunkBatch(
                self.renderer.ctx, self.renderer.shader_manager["cube"], self.position, settings.CHUNK_BLOCK_MARGIN))
            self.indices.add(
                self.renderer.scene.add_chunk_batch(self.chunkbatches[-1]))
            self.total_space += settings.CHUNK_BLOCK_MARGIN
            self.add_block(block)  # On fait une jolie petite récursion

    def delete_block(self, block):
        """
        Supprime le bloc de l'écran
        :param block: le tuple (x,y,z,type)
        """
        if block in self.cubes_dynamic:
            index = self.cubes_dynamic.index(block)
            self.cubes_dynamic_set.remove(self.cubes_dynamic.pop(index))
            to_del = self.cube_indices.pop(index)
            self.chunkbatches[to_del[0]].delete_cube(to_del[1])
