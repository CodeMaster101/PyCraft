
from game.rendering.batch import Batch
from moderngl import Context, Program, TRIANGLE_STRIP
from numpy import array, zeros, empty, uint32, int32


"""
Ce fichier contient la classe 'WaterBatch' extrêment similaire à la classe 'ChunkBatch'
C'est pourquoi ce fichier est un copié-collé du fichier 'chunk_batch', avec quelques modifications
Garder deux fichiers différents a l'avantage de permettre plus de changements et de modulabilité.

Pour plus de détails voir la classe 'ChunkBatch'.
"""


VERTEX_XYZ_COORD_LOOKUP = ((1, 1, 0), (1, 1, 0), (0, 1, 0), (1, 0, 0), (0, 0, 0), (0, 0, 1), (0, 1, 0), (0, 1, 1),
                           (1, 1, 0), (1, 1, 1), (1, 0, 0), (1, 0, 1), (0, 0, 1), (1, 1, 1), (0, 1, 1), (0, 1, 1))
VERTEX_TEX_COORD_LOOKUP = (3, 3, 1, 2, 0, 1, 2, 3, 0, 1, 2, 3, 1, 2, 0, 0)


CUBE_FORMAT = "i4"
BYTES_PER_VERTEX = 4
CUBE_ATTRIBUTES = ("PackedData",)
VERTICES_PER_CUBE = 16

CUBE_SIZE = VERTICES_PER_CUBE * BYTES_PER_VERTEX


class WaterBatch(Batch):
    def __init__(self,
                 ctx: Context,
                 program: Program,
                 chunk_coord: tuple,
                 number_of_cubes: int):

        buffer_size = number_of_cubes * CUBE_SIZE
        super().__init__(ctx, program, buffer_size, CUBE_FORMAT, CUBE_ATTRIBUTES, 0, 0)

        self.buffer_size = buffer_size
        self.next_free_index = 0
        self.other_free_indices = []

        self.program = program
        self.chunk_coordinates = array(chunk_coord, dtype=int32)

    def add_cube(self, cube_x, cube_y, cube_z):
        if len(self.other_free_indices):
            index = self.other_free_indices.pop(0)
        else:
            index = self.next_free_index
            self.next_free_index += 1

        vbo_data = empty(VERTICES_PER_CUBE, dtype=uint32)

        for i in range(VERTICES_PER_CUBE):
            xyz_offset = VERTEX_XYZ_COORD_LOOKUP[i]

            x = cube_x + xyz_offset[0]
            y = cube_y + xyz_offset[1]
            z = cube_z + xyz_offset[2]

            vbo_data[i] = (((z << 10) + (y << 5) + x) << 8) + VERTEX_TEX_COORD_LOOKUP[i]

        self.update_vbo(vbo_data, index * CUBE_SIZE)
        return index

    def push_cubes(self, cubes_data):
        vertices_per_cube = VERTICES_PER_CUBE
        vbo_data = empty(len(cubes_data) * vertices_per_cube, dtype=uint32)

        memory_offset_index = self.next_free_index
        indices = tuple(range(self.next_free_index, self.next_free_index + len(cubes_data)))
        self.next_free_index += len(cubes_data)

        for index, data in enumerate(cubes_data):
            for i in range(vertices_per_cube):
                xyz_offset = VERTEX_XYZ_COORD_LOOKUP[i]

                x = data[0] + xyz_offset[0]
                y = data[1] + xyz_offset[1]
                z = data[2] + xyz_offset[2]

                vbo_data[index * vertices_per_cube + i] = (((z << 10) + (y << 5) + x) << 8) + VERTEX_TEX_COORD_LOOKUP[i]

        self.update_vbo(vbo_data, memory_offset_index * CUBE_SIZE)
        return indices

    def edit_cube(self, cube_x, cube_y, cube_z, index):
        vbo_data = empty(VERTICES_PER_CUBE, dtype=uint32)

        for i in range(VERTICES_PER_CUBE):
            xyz_offset = VERTEX_XYZ_COORD_LOOKUP[i]

            x = cube_x + xyz_offset[0]
            y = cube_y + xyz_offset[1]
            z = cube_z + xyz_offset[2]

            vbo_data[i] = (((z << 10) + (y << 5) + x) << 8) + VERTEX_TEX_COORD_LOOKUP[i]

        self.update_vbo(vbo_data, index * CUBE_SIZE)

    def delete_cube(self, index):
        self.other_free_indices.append(index)
        self.update_vbo(zeros(VERTICES_PER_CUBE, dtype=int32), index * CUBE_SIZE)

    def render(self):
        self.program["chunk_coordinates"].write(self.chunk_coordinates)
        self.vao.render(mode=TRIANGLE_STRIP)
